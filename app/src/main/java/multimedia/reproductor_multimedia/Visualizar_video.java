package multimedia.reproductor_multimedia;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

public class Visualizar_video extends AppCompatActivity implements SurfaceHolder.Callback {
    private SeekBar sk_volumen;
    private SeekBar sk_tiempo;
    private AudioManager audioManager;
    private static final int READ_REQUEST_CODE = 42;
    private static String TAG ="VIDEO";
    private TextView tvTime;
    private MediaPlayer mp;
    private int position;
    private Handler skHandler = new Handler();
    private SurfaceView surfaceView;


    private ImageButton imageButtonAtrasar;
    private ImageButton imageButtonIniciar;
    private ImageButton imageButtonPause;
    private ImageButton imageButtonReanudar;
    private ImageButton imageButtonAdelantar;
    private ImageButton imageButtonArchivos;
    private ImageButton imageButtonPlay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_video);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        surfaceView = findViewById(R.id.surfaceViewVideo);
        sk_volumen = findViewById(R.id.seekBarVolumen);
        sk_tiempo = findViewById(R.id.seekBarTiempo);
        tvTime = findViewById(R.id.textViewTiempo);

        imageButtonAtrasar = findViewById(R.id.imageButtonAtrasar);
        imageButtonIniciar = findViewById(R.id.imageButtonSaltarInicio);
        imageButtonPause = findViewById(R.id.imageButtonPause);
        imageButtonPlay = findViewById(R.id.imageButtonPlay);
        imageButtonReanudar = findViewById(R.id.imageButtonReaundar);
        imageButtonAdelantar = findViewById(R.id.imageButtonAdelantar);
        imageButtonArchivos = findViewById(R.id.imageButtonExplorador);

        volumen();

    }



    public void performFileSearch(View view) {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("video/*");

        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {


        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();


                mp = MediaPlayer.create(this, uri);
                final SurfaceHolder surfaceHolder = surfaceView.getHolder();
                surfaceHolder.addCallback(new SurfaceHolder.Callback() {
                    @Override
                    public void surfaceCreated(SurfaceHolder surfaceHolder) {

                        int videoWidth = mp.getVideoWidth();
                        int videoHeight = mp.getVideoHeight();
                        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();

                        android.view.ViewGroup.LayoutParams lp = surfaceView.getLayoutParams();
                        lp.width = screenWidth;
                        lp.height = (int) (((float)videoHeight / (float)videoWidth) * (float)screenWidth);

                        surfaceView.setLayoutParams(lp);

                        mp.setDisplay(surfaceHolder);
                        mp.start();
                    }

                    @Override
                    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

                    }

                    @Override
                    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

                    }
                });

                tvTime.setText( getHRM(mp.getDuration()) );
                sk_tiempo.setMax(mp.getDuration());
                sk_tiempo.setProgress(mp.getCurrentPosition());
                skHandler.postDelayed(updateskSong, 1000);

            }
        }
    }


    public void pulsarPlay(View view){

        if(mp == null){
            Snackbar.make(view, getString(R.string.selecion_cancion_video), Snackbar.LENGTH_LONG).show();
        }else
        if (mp != null && mp.isPlaying()){
            Snackbar.make(view, getString(R.string.reproduciendose_video), Snackbar.LENGTH_LONG).show();
        }else{
            Snackbar.make(view, getString(R.string.play), Snackbar.LENGTH_LONG).show();
            mp.start();

        }


    }

    public void pulsarpause(View view){

        if(mp == null){
            Snackbar.make(view, getString(R.string.selecion_video), Snackbar.LENGTH_LONG).show();
        }
        else
        if( mp != null && mp.isPlaying()) {
            mp.pause();
            position = mp.getCurrentPosition();
            Snackbar.make(view, getString(R.string.play_pause), Snackbar.LENGTH_LONG).show();
        }


    }

    public void saltarInicio(View view) {

        if(mp == null){
            Snackbar.make(view, getString(R.string.selecion_video), Snackbar.LENGTH_LONG).show();
        }
        else
        if (mp != null && mp.isPlaying()) {

            mp.pause();
            position = 0;
            mp.seekTo(position);
            mp.start();
            Snackbar.make(view, getString(R.string.play_inicio), Snackbar.LENGTH_LONG).show();
        }



    }

    public void pulsarContinuar(View view){
        if (mp != null && mp.isPlaying() == false) {
            mp.seekTo(position);
            mp.start();

        }

        Snackbar.make(view, getString(R.string.play_continuar), Snackbar.LENGTH_LONG).show();
    }

    Runnable updateskSong = new Runnable() {
        @Override
        public void run() {
            sk_tiempo.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    if(b){
                        mp.seekTo(i);
                        seekBar.setProgress(i);
                    }else{

                    }

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
            sk_tiempo.setProgress( mp.getCurrentPosition() );
            tvTime.setText(getHRM(mp.getCurrentPosition())+ " - " +getHRM(mp.getDuration()));
            skHandler.postDelayed(updateskSong, 1000);

        }
    };



    private String getHRM(int milliseconds )
    {
        int seconds = (int) (milliseconds / 1000) % 60 ;
        int minutes = (int) ((milliseconds / (1000*60)) % 60);
        int hours   = (int) ((milliseconds / (1000*60*60)) % 24);
        return ((hours<10)?"0"+hours:hours) + ":" +
                ((minutes<10)?"0"+minutes:minutes) + ":" +
                ((seconds<10)?"0"+seconds:seconds);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mp != null) {
            mp.stop();
            position = 0;
        }
    }


    public void pulsarAlante(View view){

        final int TIME = 5000;

        if(mp == null){
            Snackbar.make(view, getString(R.string.selecion_video), Snackbar.LENGTH_LONG).show();
        }
        else
        if (mp != null && mp.isPlaying()) {
            position = mp.getCurrentPosition();
            mp.seekTo(position+TIME);
            mp.start();
            Snackbar.make(view, getString(R.string.adelantar), Snackbar.LENGTH_LONG).show();
        }

    }

    public void pulsarAtras(View view){

        final int TIME = 5000;

        if(mp == null){
            Snackbar.make(view, getString(R.string.selecion_video), Snackbar.LENGTH_LONG).show();
        }
        else if (mp != null && mp.isPlaying()) {
            position = mp.getCurrentPosition();
            mp.seekTo(position-TIME);
            mp.start();
            Snackbar.make(view, getString(R.string.atrasar), Snackbar.LENGTH_LONG).show();
        }


    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    //SUBIR VOLUMEN DE LA CANCION
    public void volumen() {
        try {

            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            sk_volumen.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            sk_volumen.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

            sk_volumen.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override            public void onStopTrackingTouch(SeekBar arg0) {
                }

                @Override            public void onStartTrackingTouch(SeekBar arg0) {
                }

                @Override            public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}