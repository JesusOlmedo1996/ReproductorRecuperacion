package multimedia.reproductor_multimedia;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

public class ReproductorMusica extends AppCompatActivity {
    private SeekBar sk_volumen;
    private SeekBar sk_tiempo;
    private AudioManager audioManager;
    private MediaPlayer mediaPlayer;
    private static final int READ_REQUEST_CODE = 42;
    private static String TAG ="MUSICA";
    private TextView tvTime;
    private TextView tvTime2;
    private MediaPlayer mp;
    private MediaRecorder mr;
    private int position;
    private ImageView imageDisco;
    private Handler skHandler = new Handler();

    private ImageButton imageButtonAtrasar;
    private ImageButton imageButtonIniciar;
    private ImageButton imageButtonPause;
    private ImageButton imageButtonPlay;
    private ImageButton imageButtonReanudar;
    private ImageButton imageButtonAdelantar;
    private ImageButton imageButtonArchivos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reproductor_musica);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        sk_volumen = findViewById(R.id.seekBarVolumen);
        sk_tiempo = findViewById(R.id.seekBarTiempo);
        tvTime = findViewById(R.id.textViewTiempo);
        imageDisco = findViewById(R.id.imageViewDisco);

        imageButtonAtrasar = findViewById(R.id.imageButtonAtrasar);
        imageButtonIniciar = findViewById(R.id.imageButtonSaltarInicio);
        imageButtonPause = findViewById(R.id.imageButtonPause);
        imageButtonPlay = findViewById(R.id.imageButtonPlay);
        imageButtonReanudar = findViewById(R.id.imageButtonReaundar);
        imageButtonAdelantar = findViewById(R.id.imageButtonAdelantar);
        imageButtonArchivos = findViewById(R.id.imageButtonExplorador);

        volumen();



    }

    public void performFileSearch(View view) {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("audio/*");

        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                Log.i(TAG, "Uri: " + uri.toString());
                pulsarStartStorage(uri);

                tvTime.setText( getHRM(mp.getDuration()) );
                sk_tiempo.setMax(mp.getDuration());
                sk_tiempo.setProgress(mp.getCurrentPosition());
                skHandler.postDelayed(updateskSong, 1000);
            }
        }
    }

    public void volumen() {
        try {

            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            sk_volumen.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            sk_volumen.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

            sk_volumen.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override            public void onStopTrackingTouch(SeekBar arg0) {
                }

                @Override            public void onStartTrackingTouch(SeekBar arg0) {
                }

                @Override            public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void pulsarStartStorage(Uri uri){
        String TAG = "StartStorage";

        Log.d(TAG, Environment.getExternalStorageDirectory().toString());
        Log.d(TAG, Environment.DIRECTORY_MUSIC);


        Log.d(TAG, uri.toString());
        if( mp != null && mp.isPlaying()) {
            mp.pause();
            mp = MediaPlayer.create(this, uri);
            mp.start();
        }else{
            mp = MediaPlayer.create(this, uri);
            mp.start();

        }


    }

    public void pulsarPlay(View view){

        if(mp == null){
            Snackbar.make(view, getString(R.string.selecion_cancion_video), Snackbar.LENGTH_LONG).show();
        }else
        if (mp != null && mp.isPlaying()){
            Snackbar.make(view, getString(R.string.reproduciendose_video), Snackbar.LENGTH_LONG).show();
        }else{
            mp.start();
            //LE PONGO QUE CUANDO LE DOY AL PLAY EL DISCO DE VUELTAS
            Snackbar.make(view, getString(R.string.play), Snackbar.LENGTH_LONG).show();
        }


    }

    public void pulsarpause(View view){

        if(mp == null){
            Snackbar.make(view, getString(R.string.selecion_cancion_video), Snackbar.LENGTH_LONG).show();
        }
        else
        if( mp != null && mp.isPlaying()) {
            mp.pause();
            position = mp.getCurrentPosition();
            Snackbar.make(view, getString(R.string.play_pause), Snackbar.LENGTH_LONG).show();
            ;        }


    }

    public void saltarInicio(View view) {
        if(mp == null){
            Snackbar.make(view, getString(R.string.selecion_cancion_video), Snackbar.LENGTH_LONG).show();
        }
        else
        if (mp != null && mp.isPlaying()) {

            mp.pause();
            position = 0;
            mp.seekTo(position);
            mp.start();
            Snackbar.make(view, getString(R.string.play_inicio), Snackbar.LENGTH_LONG).show();
        }


    }

    public void pulsarContinuar(View view){

        if (mp != null && mp.isPlaying() == false) {
            mp.seekTo(position);
            mp.start();
        }

        Snackbar.make(view, getString(R.string.play_continuar), Snackbar.LENGTH_LONG).show();
    }

    Runnable updateskSong = new Runnable() {
        @Override
        public void run() {
            sk_tiempo.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    if(b){
                        mp.seekTo(i);
                        seekBar.setProgress(i);
                    }else{

                    }

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
            sk_tiempo.setProgress( mp.getCurrentPosition() );
            tvTime.setText(getHRM(mp.getCurrentPosition())+ " - " +getHRM(mp.getDuration()));
            skHandler.postDelayed(updateskSong, 1000);

        }
    };



    private String getHRM(int milliseconds )
    {
        int seconds = (int) (milliseconds / 1000) % 60 ;
        int minutes = (int) ((milliseconds / (1000*60)) % 60);
        int hours   = (int) ((milliseconds / (1000*60*60)) % 24);
        return ((hours<10)?"0"+hours:hours) + ":" +
                ((minutes<10)?"0"+minutes:minutes) + ":" +
                ((seconds<10)?"0"+seconds:seconds);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mp != null) {
            mp.stop();
            position = 0;
        }
    }

    public void pulsarAlante(View view){

        final int TIME = 5000; //ms

        if(mp == null){
            Snackbar.make(view, getString(R.string.selecion_cancion_video), Snackbar.LENGTH_LONG).show();
        }
        else
        if (mp != null && mp.isPlaying()) {
            position = mp.getCurrentPosition();
            mp.seekTo(position+TIME);
            mp.start();
            Snackbar.make(view, getString(R.string.adelantar), Snackbar.LENGTH_LONG).show();
        }

    }

    public void pulsarAtras(View view){

        final int TIME = 5000; //ms


        if(mp == null){
            Snackbar.make(view, getString(R.string.selecion_cancion_video), Snackbar.LENGTH_LONG).show();
        }
        else if (mp != null && mp.isPlaying()) {
            position = mp.getCurrentPosition();
            mp.seekTo(position-TIME);
            mp.start();
            Snackbar.make(view, getString(R.string.atrasar), Snackbar.LENGTH_LONG).show();
        }


    }
}

